#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int leerYValidar(int);
char
 leerYValidarCond();
void counttingValleys(int[], int);

int main ()
{	

	int cantidad=0;
	int ruta[cantidad];
	
	printf("\n\t\t EJERCICIO RESUELTO POR EL DESARROLLADOR JOSE DOMINGUEZ\n");
	printf("****************************************************************************************\n\n\n");
	
	printf("Ingrese la cantidad de pasos\n");
	cantidad = leerYValidar(0);
		
	counttingValleys(ruta, cantidad);
		
return 0;
}

void counttingValleys(int v[], int n){
	
	int i;
	char letra;
	
	printf("\nUsted realizo un total de %d pasos\n", n);
	printf("Digite el estado de cada unidad de pasos [letra U cuesta arriba, letra D descenso]\n");
	
	for(i=0; i<n; i++){
		
		letra = leerYValidarCond();
		v[i] = letra;
		
	}
	
	printf("\nFormato:\n");
	
	printf("-------------------------------\n");
	for(i=0; i<n; i++){
		printf("%c", v[i]);
	}
	printf("\n-------------------------------\n");
	
}

char leerYValidarCond(){
	
	int band=0;
	char dato;
	
	do{
		
		if(band==0){
			band=1;
		}else{
			printf("\nError, digite un estado valida [D o U]\n");
		}
		fflush(stdin);
		scanf("%c", &dato);
	}while(dato != 'd' && dato != 'u');
	
	return dato;
	
}

int leerYValidar(int nro){
	
	int band=0, dato;
	
	do{
		
		if(band==0){
			band=1;
		}else{
			printf("\nError, ingrese una cantidad mayor a 0\n");
		}	
		scanf("%d", &dato);
	}while(dato < nro);
	
	return dato;
	
}
