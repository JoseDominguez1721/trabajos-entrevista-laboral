#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void catAndMouse(int, int, int);
int leerYValidar(int, int);

int main ()
{	

	int x;
	int y;
	int z;

	printf("\n\t\t EJERCICIO RESUELTO POR EL DESARROLLADOR JOSE DOMINGUEZ\n");
	printf("****************************************************************************************\n\n\n");

	printf("Ingrese la posicion del Cat A\n");
		x = leerYValidar(0, 100);
		
	printf("Ingrese la posicion del Cat B\n");
		y = leerYValidar(0, 100);
		
	printf("Ingrese la posicion del mouse C\n");
		z = leerYValidar(0, 100);
	
	catAndMouse(x, y, z);
		
return 0;
}

void catAndMouse(int a, int b, int c){
	
	int i;
	int distX=0;
	int distY=0;
	
	if(a<c){
		for(i=a; i<c; i++)
		distX++;
	}else{
		for(i=a; i>c; i--)
		distX++;
	}
	
	if(b<c){
		for(i=b; i<c; i++)
		distY++;
	}else{
		for(i=b; i>c; i--)
		distY++;
	}
	
	if(distX == distY){
		printf("\n\n---------------------------------------------\n");
		printf("Los gatos A y B estan en la misma distancia, se estan peleando\n");
		printf("---------------------------------------------\n");	
	}else if(distX < distY){
		printf("\n\n---------------------------------------------\n");
		printf("El gato A atrapo al raton\n");
		printf("---------------------------------------------\n");
	}else if(distY < distX){
		printf("\n\n---------------------------------------------\n");
		printf("El gato B atrapo al raton\n");
		printf("---------------------------------------------\n");
	}	
	
}

int leerYValidar(int nro, int nroDos){
	
	int dato;
	
	int band=0;
	
	do{
		
		if(band==0){
			band=1;
		}else{
			printf("Error, digite una posicion [1< x >100]\n");
		}
		scanf("%d", &dato);
	}while(dato < nro || dato > nroDos);
	
	return dato;
	
}
